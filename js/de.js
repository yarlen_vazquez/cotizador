function Limpiar() {

    var lblEnganche = document.getElementById('lblEnganche');
    lblEnganche.innerHTML = "Enganche: $";

    var lbltotal = document.getElementById('lbltotal');
    lbltotal.innerHTML = "Total a Financiar: $";

    var lblPago = document.getElementById('lblPago');
    lblPago.innerHTML = "Pago Mensual: $";

}


function Calcular() {
    Limpiar();

    var valor = document.getElementById('valor').value;
    var enganche = (valor * 0.30)
    var total = 0;
    var meses = document.getElementById('meses').value;
    var intereses = 0;
    var pagomen = 0;

    var lblenganche = document.getElementById('lblEnganche');
    console.log("Enganche: " + enganche);
    lblenganche.innerHTML = lblenganche.innerHTML + enganche;

    if (meses == 12) {
        intereses = (valor - enganche) * 0.125;
        console.log("Intereses: " + intereses);
    } else if (meses == 18) {
        intereses = (valor - enganche) * 0.172;
        console.log("Intereses: " + intereses);
    } else if (meses == 24) {
        intereses = (valor - enganche) * 0.21;
        console.log("Intereses: " + intereses);
    } else if (meses == 36) {
        intereses = (valor - enganche) * 0.26;
        console.log("Intereses: " + intereses);
    } else if (meses == 48) {
        intereses = (valor - enganche) * 0.45;
        console.log("Intereses: " + intereses);
    }


    var lbltotal = document.getElementById('lbltotal');
    total = valor - enganche + intereses;
    lbltotal.innerHTML = lbltotal.innerHTML + total;


    var lblPago = document.getElementById('lblPago');
    pagomen = total / meses;
    lblPago.innerHTML = lblPago.innerHTML + pagomen;

}